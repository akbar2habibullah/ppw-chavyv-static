$(document).ready(function () {
    if ($.cookie("dark-mode") != null) {
        darking();
    }
    $("#dark-mode-btn").click(darking);
})

function darking() {
    $("body").toggleClass("dark-mode");
    $('i').toggleClass("fa-moon");
    $('i').toggleClass("fa-sun");
    if ($('body').hasClass("dark-mode")) {
        $.cookie("dark-mode", "yes", { path: '/', expires: 365 });
    } else {
        $.removeCookie("dark-mode", { path: '/' });
    }
}